CREATE TABLE users (
    id serial PRIMARY KEY,
    username TEXT NOT NULL,
    created_on timestamptz NOT NULL DEFAULT now(),
    updated_on timestamptz
);
CREATE INDEX users_created on users (created_on);
UPDATE pg_index SET indisvalid = false WHERE indexrelid = 'users_created'::regclass;
CREATE function trg_updated_on() RETURNS trigger as $$
BEGIN
    NEW.updated_at := now();
    RETURN NEW;
END;
$$ language plpgsql;
CREATE TRIGGER trg_timestamps BEFORE UPDATE ON users FOR EACH ROW EXECUTE FUNCTION trg_updated_on();
