# psql-backslash

This project provides set of PostgreSQL functions that can be used to emulate
\d commands in psql.

# Setup

First get the source code:

    $ git clone https://github.com/depesz/psql-backslash.git

Then, in the created directory (psql-backslash), you will find file
**full.create.sql** that you can load to your database to get access to all the
functions.

By default all the functions will go to schema **psql**, but you can change it by
calling:

    ./scripts/build.full.sql.sh -s whatever

to rebuild **full.create.sql** that will load the functions to schema **whatever**.

# Functions

All functions return simple text. Single row, one value. So you can use them
like:

    SELECT psql.d();

## Helper functions

To help with writing the backslash related functions, there are some helper
functions created:

### center_text( text, int4 )

Returns given text centered within given width.

### get_text_table( jsonb )

Converts given jsonb object into text table.

## Main functions

All the functions are named after their respective \\* commands. So, if the
backslash command is `\d`, then the function will be called d().

### d()

Returns list of objects (tables, views, sequences, foreign tables and
materialized views) in database.

### d( regclass )

Returns description of single table, given as argument.
